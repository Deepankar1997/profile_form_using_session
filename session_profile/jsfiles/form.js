$(function(){
	var $validateform = $("#profile_form");
	if($validateform.length){
		$validateform.validate({
			rules:{
				fname: {
					required: true
				},
				lname: {
					required: true
				},
				dob: {
					required: true
				},
				gender: {
					required: true
				},
				emailaddress: {
					required: true,
					email: true
				},
				contactnumber: {
					required: true
				},
				qualifications: {
					required: true
				},
			},
			messages:{
				fname: {
					required: 'Please enter the first name'
				},
				lname: {
					required: 'Please enter the last name'
				},
				dob: {
					required: 'Please enter the date of birth'
				},	
				gender: {
					required: 'Please enter the gender'
				},
				emailaddress: {
					required: 'Please enter the email address',
					email: 'Please enter a valid email address'
				},
				contactnumber: {
					required: 'Please enter the contact number'
				},
				qualifications: {
					required: 'Please enter the qualifications'
				},
			},
		})
	}
})