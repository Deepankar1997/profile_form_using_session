<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="cssfiles/form.css">
	<title>Profile</title>
</head>
<body>
	<?php
	//$data=$_SESSION['data'];
	$data=(isset($_SESSION['flag']) && ($_SESSION['flag']) && is_array($_SESSION['data']))?$_SESSION['data']:array();
	switch($_SESSION['flag'])
	{
		case "1":$showerror="First Name required";
		break;
		case "2":$showerror="Only alphabets allowed";
		break;
		case "3":$showerror="Only alphabets allowed";
		break;
		case "4":$showerror="Last Name required";
		break;
		case "5":$showerror="Only alphabets allowed";
		break;
		case "6":$showerror="Date of Birth required";
		break;
		case "7":$showerror="Gender required";
		break;
		case "8":$showerror="Email required";
		break;
		case "9":$showerror="Invalid email format";
		break;
		case "10":$showerror="Contact Number required";
		break;
		case "11":$showerror="Invalid phone number format.Must be of 10 digits.";
		break;
		case "12":$showerror="Invalid URL";
		break;
		case "13":$showerror="Qualifications required";
		break;
		default:$showerror="";
	}
	?>

	<div class="container my-5">
		<div>
			<h1>Enter Your Details Here !</h1>
		</div>
		<hr>
		<form id="profile_form" method="post" novalidate action="profile_page.php" enctype="multipart/form-data">
			<h5>Personal Details:</h5>
			<hr>
			<div class="py-3">
				<div class="form-group">
					<label>Name<span class="text-danger">*</span></label>
					<div class="row justify-content-between">
						<div class="col">
							<input type="text" class="form-control" name="fname" placeholder="First name" value="<?php echo $data["fname"];?>">
							<span class="error"><?php if($_SESSION['flag']=="1" or $_SESSION['flag']=="2"){echo $showerror;}?></span>
						</div>
						<div class="col">
							<input type="text" class="form-control" name="mname" placeholder="Middle name" value="<?php echo $data["mname"];?>">
							<span class="error"><?php if($_SESSION['flag']=="3"){echo $showerror;}?></span>
						</div>
						<div class="col">
							<input type="text" class="form-control" name="lname" placeholder="Last name" value="<?php echo $data["lname"];?>">
							<span class="error"><?php if($_SESSION['flag']=="4" or $_SESSION['flag']=="5"){echo $showerror;}?></span>
						</div>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-6">
						<label for="dob">Date of Birth<span class="text-danger">*</span></label>
						<input type="date" class="form-control" name="dob" id="dob" max="<?php echo date('Y-m-d');?>" value="<?php echo $data["dob"];?>">
						<span class="error"><?php if($_SESSION['flag']=="6"){echo $showerror;}?></span>
					</div>
					<div class="col-6">
						<label>Gender<span class="text-danger">*</span></label>
						<div class="input-group row justify-content-between px-3 pt-2">
							<div><input type="radio" name="gender" id="radio1" value="Male"><label for="radio1">Male</label></div>
							<div><input type="radio" name="gender" id="radio2" value="Female"><label for="radio2">Female</label></div>
							<div><input type="radio" name="gender" id="radio3" value="Others"><label for="radio3">Others</label></div>
						</div>
						<span class="error"><?php if($_SESSION['flag']=="7"){echo $showerror;}?></span>
					</div>
				</div>
				<label>Profile photo</label>
				<div>
					<input type="file" name="profile_pic" id="profile_pic">
				</div>
			</div>
			<hr>
			<h5>Contact Details:</h5>
			<hr>
			<div class="py-3">
				<div class="form-group">
					<label for="emailaddress">Email address<span class="text-danger">*</span></label>
					<input type="email" class="form-control" name="emailaddress" placeholder="xyz@gmail.com" value="<?php echo $data["emailaddress"];?>">
					<span class="error"><?php if($_SESSION['flag']=="8" or $_SESSION['flag']=="9"){echo $showerror;}?></span>
				</div>
				<div class="form-group">
					<label for="contactnumber">Contact Number<span class="text-danger">*</span></label>
					<input type="number" class="form-control" name="contactnumber" value="<?php echo $data["contactnumber"];?>">
					<span class="error"><?php if($_SESSION['flag']=="10" or $_SESSION['flag']=="11"){echo $showerror;}?></span>
				</div>
				<div class="form-group">
					<label for="address">Address</label>
					<textarea class="form-control" name="address" rows="3" placeholder="House no.,Street,PO/PS" value="<?php echo $data["address"];?>"></textarea>
				</div>
				<div class="form-group">
					<label for="p_links">Professional Links</label>
					<input type="text" class="form-control" name="p_links" placeholder="Ex-: www.hackerrank.com" value="<?php echo $data["p_links"];?>">
					<span class="error"><?php if($_SESSION['flag']=="12"){echo $showerror;}?></span>
				</div>
			</div>
			<hr>
			<h5>Other Details:</h5>
			<hr>
			<div class="py-3">
				<div class="row">
					<div class="col-6">
						<div class="form-group">
							<label for="qualifications">Educational Qualifications<span class="text-danger">*</span></label>
							<select class="form-control" name="qualifications">
								<option hidden>Choose highest qualification</option>
								<option value="Matriculation">Matriculation</option>
								<option value="Under-Graduate">Under Graduate</option>
								<option value="Graduate">Graduate</option>
								<option value="Post-Graduate">Post Graduate</option>
								<option value="Doctorate">Doctorate</option>
							</select>
							<span class="error"><?php if($_SESSION['flag']=="13"){echo $showerror;}?></span>
						</div>
						<div class="form-group">
							<label>Interest Areas</label>
							<select class="form-control selectpicker" multiple data-live-search="true" name="interests[]">
								<option hidden>Choose areas of interests</option>
								<option value="Sports">Sports</option>
								<option value="Travelling">Travelling</option>
								<option value="Painting">Painting</option>
								<option value="Gaming">Gaming</option>
								<option value="Cooking">Cooking</option>
								<option value="Music">Music</option>
								<option value="Blogging">Blogging</option>
							</select>
						</div>
					</div>
					<div class="col-6">
						<label>Skills</label>
						<div class="input-group justify-content-between">
							<div><input type="checkbox" id="option1" name="skills[]" value="C/C++"><label for="option1">C/C++</label></div>
							<div><input type="checkbox" id="option2" name="skills[]" value="Java"><label for="option2">Java</label></div>
							<div><input type="checkbox" id="option3" name="skills[]" value="PHP"><label for="option3">PHP</label></div>
							<div><input type="checkbox" id="option4" name="skills[]" value="HTML"><label for="option4">HTML</label></div>
							<div><input type="checkbox" id="option5" name="skills[]" value="CSS"><label for="option5">CSS</label></div>
							<div><input type="checkbox" id="option6" name="skills[]" value="JavaScript"><label for="option6">JavaScript</label></div>
							<div><input type="checkbox" id="option7" name="skills[]" value="MySQL"><label for="option7">MySQL</label></div>
							<div><input type="checkbox" id="option8" name="skills[]" value="MongoDB"><label for="option8">MongoDB</label></div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<h5>About User:</h5>
			<hr>
			<div class="py-3">
				<div class="form-group">
					<label for="about">About</label>
					<textarea class="form-control" name="about" rows="3" placeholder="Please tell something about yourself..." value="<?php echo $data["about"];?>"></textarea>
				</div>
			</div>
			<div class="py-5">
				<input class="btn btn-primary" type="submit" value="Submit">
			</div>
		</form>
	</div>
	<?php
	session_unset();
	?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
	<!-- <script src="jsfiles/form.js"></script> -->
</body>
</html>