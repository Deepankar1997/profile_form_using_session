<?php
session_start()
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="cssfiles/profile.css">
	<title>Profile Page</title>
</head>
<body>
	<?php
	$_SESSION['fname'] = $_SESSION['mname'] = $_SESSION['lname'] = $_SESSION['dob'] = $_SESSION['gender'] = $_SESSION['emailaddress'] = $_SESSION['contactnumber'] = $_SESSION['qualifications'] = $_SESSION['p_links'] = $_SESSION['address'] = $_SESSION['skills'] = $_SESSION['interests'] = $_SESSION['about'] = "";

	if ($_SERVER["REQUEST_METHOD"] == "POST") 
	{
	//Validating Qualifications
		if ($_POST["qualifications"]=="Choose highest qualification") 
		{
			$_SESSION['flag']=13;
		} 
		else 
		{
			$_SESSION['qualifications'] = test_input($_POST["qualifications"]);
		}
	//Validating Professional links
		$_SESSION['p_links'] = test_input($_POST["p_links"]);
		if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$_SESSION['p_links']) and !empty($_SESSION['p_links'])) 
		{
			$_SESSION['flag']=12;
		}
	//Validating Contact Number
		if (empty($_POST["contactnumber"])) 
		{
			$_SESSION['flag']=10;
		} 
		else 
		{
			$_SESSION['contactnumber'] = test_input($_POST["contactnumber"]);
			if (strlen($_SESSION['contactnumber'])!=10 and $contactnumber>0) 
			{
				$_SESSION['flag']=11;
			}
		}
	//Validating Email Address
		if (empty($_POST["emailaddress"])) 
		{
			$_SESSION['flag']=8;
		} 
		else 
		{
			$_SESSION['emailaddress'] = test_input($_POST["emailaddress"]);
			if (!filter_var($_SESSION['emailaddress'], FILTER_VALIDATE_EMAIL)) 
			{
				$_SESSION['flag']=9;
			}
		}
	//Validating Gender
		if (empty($_POST["gender"])) 
		{
			$_SESSION['flag']=7;
		} 
		else 
		{
			$_SESSION['gender'] = test_input($_POST["gender"]);
		}
	//Validating Date of Birth
		if (empty($_POST["dob"])) 
		{
			$_SESSION['flag']=6;
		} 
		else 
		{
			$_SESSION['dob'] = test_input($_POST["dob"]);
		}
	//Validating Last name
		if (empty($_POST["lname"])) 
		{
			$_SESSION['flag']=4;
		} 
		else 
		{
			$_SESSION['lname'] = test_input($_POST["lname"]);
			if (!preg_match("/^[a-zA-Z-']*$/",$_SESSION['lname'])) 
			{
				$_SESSION['flag']=5;
			}
		}
	//Validating Middle Name
		$_SESSION['mname'] = test_input($_POST["mname"]);
		if (!preg_match("/^[a-zA-Z-']*$/",$_SESSION['mname'])) 
		{
			$_SESSION['flag']=3;
		}
	//Validating First Name
		if (empty($_POST["fname"])) 
		{
			$_SESSION['flag']=1;
		} 
		else 
		{
			$_SESSION['fname'] = test_input($_POST["fname"]);
			if (!preg_match("/^[a-zA-Z-']*$/",$_SESSION['fname'])) 
			{
				$_SESSION['flag']=2;

			}
		}
	//Redirecting if any validations if getting false value
		if($_SESSION['flag']!=0)
		{
			$_SESSION['data']=$_POST;
			header('Location: index.php');
		}
		//Sanitizing the remaining data
		$_SESSION['address'] = test_input($_POST["address"]);
		$_SESSION['skills'] = filter_var_array($_POST["skills"]);
		$_SESSION['interests'] = filter_var_array($_POST["interests"]);
		$_SESSION['about'] = test_input($_POST['about']);
	}
	function test_input($data) 
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}	
	?>
	<?php
	$target_dir = "uploads/";
	$target_file = $target_dir . basename($_FILES["profile_pic"]["name"]);
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	$status="";

	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"]))
	{
		$check = getimagesize($_FILES["profile_pic"]["tmp_name"]);
		if($check !== false)
		{
			$status="File is an image - " . $check["mime"] . ".";
			$uploadOk = 1;
		} 
		else 
		{
			$status="File is not an image.";
			$uploadOk = 0;
		}
	}

	// Check if file already exists
	if (file_exists($target_file))
	{
		$status="Sorry, file already exists.";
		$uploadOk = 0;
	}

	// Check file size
	if ($_FILES["profile_pic"]["size"] > 500000) 
	{
		$status="Sorry, your file is too large.";
		$uploadOk = 0;
	}

	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) 
	{
		$status="Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$uploadOk = 0;
	}

	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) 
	{
		$status="Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} 
	else 
	{
		if (move_uploaded_file($_FILES["profile_pic"]["tmp_name"], $target_file))
		{
			$status="The file ". htmlspecialchars( basename( $_FILES["profile_pic"]["name"])). " has been uploaded.";
		} 
		else 
		{
			$status="Sorry, there was an error uploading your file.";
		}
	}
	?>
	<div class="container">
		<div class="py-3">
			<h3>Personal Details:</h3>
			<hr>
			<div class="row py-2">
				<div class="col-4 bold">
					Full Name:
				</div>
				<div class="col-8">
					<?php echo $_SESSION['fname'].' '.$_SESSION['mname'].' '.$_SESSION['lname'] ?>
				</div>
			</div>
			<div class="row py-2">
				<div class="col-4 bold">
					Date of Birth:
				</div>
				<div class="col-8">
					<?php echo $_SESSION['dob']; ?>
				</div>
			</div>
			<div class="row py-2">
				<div class="col-4 bold">
					Gender:
				</div>
				<div class="col-8">
					<?php echo $_SESSION['gender']; ?>
				</div>
			</div>
			<div class="row py-2">
				<div class="col-4 bold">
					Profile picture:
				</div>
				<div class="col-8">
					<img src="<?php echo $target_file;?>" alt="Profile pic not displayed.">
				</div>
			</div>
			<div class="row py-2">
				<div class="col-4 bold">
					Uploading Status:
				</div>
				<div class="col-8">
					<?php echo $status; ?>
				</div>
			</div>
		</div>
		<div class="py-3">
			<h3>Contact Details:</h3>
			<hr>
			<div class="row py-2">
				<div class="col-4 bold">
					Email Id:
				</div>
				<div class="col-8">
					<?php echo $_SESSION['emailaddress']; ?>
				</div>
			</div>
			<div class="row py-2">
				<div class="col-4 bold">
					Contact Number:
				</div>
				<div class="col-8">
					<?php echo $_SESSION['contactnumber']; ?>
				</div>
			</div>
			<div class="row py-2">
				<div class="col-4 bold">
					Address:
				</div>
				<div class="col-8">
					<?php echo $_SESSION['address']; ?>
				</div>
			</div>
			<div class="row py-2">
				<div class="col-4 bold">
					Professional Links:
				</div>
				<div class="col-8">
					<?php echo $_SESSION['p_links']; ?>
				</div>
			</div>
		</div>
		<div class="py-3">
			<h3>Other Details:</h3>
			<hr>
			<div class="row py-2">
				<div class="col-5 bold">
					Educational Qualifications:
				</div>
				<div class="col-7 p-0">
					<?php echo $_SESSION['qualifications']; ?>
				</div>
			</div>
			<div class="row py-2">
				<div class="col-5 bold">
					Skills:
				</div>
				<div class="col-7">
					<?php 
					foreach($_SESSION['skills'] as $s) {
						echo "<div class='row py-2'>".$s."</div>";
					}
					?>
				</div>
			</div>
			<div class="row py-2">
				<div class="col-5 bold">
					Interests:
				</div>
				<div class="col-7">
					<?php 
					foreach($_SESSION['interests'] as $i) {
						echo "<div class='row py-2'>".$i."</div>";
					}
					?>
				</div>
			</div>
		</div>
		<div class="py-3">
			<h3>About:</h3>
			<hr>
			<?php echo $_SESSION['about']; ?><br>
		</div>
	</div>
	<?php
	if (!isset($_SESSION['flag']))
	{
		session_destroy();
	}
	?>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>